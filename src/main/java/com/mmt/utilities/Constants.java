package com.mmt.utilities;

public class Constants {

    public static final String BASE_URL = "https://www.makemytrip.com/";
    public static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static final String OS_NAME = System.getProperty("os.name");
    public static final String DRIVERS_DIRECTORY = "//drivers//";
    public static final String BROWSER =  "chrome";   // for ff type "firefox" , for chrome type "chrome", for internet explorer type "ie"
    public static final String GECKO_DRIVER_KEY = "webdriver.gecko.driver";
    public static final String CHROME_DRIVER_KEY = "webdriver.chrome.driver";
    public static final String IE_DRIVER_KEY = "webdriver.ie.driver";
    public static final String GECKO_DRIVER_VALUE = "geckodriver";
    public static final String CHROME_DRIVER_VALUE = "chromedriver";
    public static final String IE_DRIVER_VALUE = "IEDriverServer";
    public static final String DEFAULT_USERNAME = "s-arabits";
    public static final String DEFAULT_PASSWORD = "12345";
}