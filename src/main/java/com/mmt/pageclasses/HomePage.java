package com.mmt.pageclasses;

import com.mmt.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public WebDriver driver;

    private String WELCOME_IMAGE = "xpath=>.//div[@class='home-page-header__message']";
    private String HOME_LINK = "xpath=>.//div[@class='navigation__container']/a//div[text()='Home']";
    private String CLASSES_LINK = "xpath=>.//div[@class='navigation__container']/a//div[text()='Classes']";
    private String ACCOUNT_ICON = "xpath=>.//*[@id='navigation_profile']/img";
    private String LOGOUT_LINK = "xpath=>.//bdi[text()='Log out']";

    public Boolean isUserLoggedIn() {
        try {
            WebElement accountIcon = waitForElement(ACCOUNT_ICON, 3);
//            WebElement welcomeImage = driver.findElement(By.xpath(ACCOUNT_ICON));
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void logOut() {
        elementClick(ACCOUNT_ICON, "Account Icon");
        WebElement logoutLink = waitForElement(LOGOUT_LINK, 3);
        javascriptClick(logoutLink, "Log Out Link");
    }

    public void navigateOverTabs(String tabName) {
        driver.findElement(By.xpath(".//div[@class='navigation__container']/a//div[text()='" + tabName + "']")).click();
    }

    public ClassesPage clickClassesTab() {
        WebElement classtab = waitForElementToBeClickable(CLASSES_LINK, 3);
        elementClick(classtab, "Classes Tab on Homepage");
        return new ClassesPage(driver);
    }
}
