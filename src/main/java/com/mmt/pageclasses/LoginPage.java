package com.mmt.pageclasses;

import com.mmt.base.BasePage;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    /***
     * Variables
     * Locators
     * URL
     */
    public WebDriver driver;
    private String EMAIL_FIELD = "id=>username";
    private String PASSWORD_FIELD = "id=>password";
    private String LOG_IN_BUTTON = "xpath=>.//button[@Type='submit']";

    /***
     * Methods
     */
    public HomePage signInWith(String email, String password) {
        waitForLoading(EMAIL_FIELD,3);
        sendData(EMAIL_FIELD,email,"Email field");
        sendData(PASSWORD_FIELD,password,"Email field");
        elementClick(LOG_IN_BUTTON,"Login Button");

        return new HomePage(driver);
    }
}
