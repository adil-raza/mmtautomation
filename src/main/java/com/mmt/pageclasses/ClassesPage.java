package com.mmt.pageclasses;

import com.mmt.base.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class ClassesPage extends BasePage {

    public ClassesPage(WebDriver driver) {
        super(driver);
        this.driver = driver;
    }

    public WebDriver driver;
    private JavascriptExecutor js;
    private String CATEGORY_DROPDOWN = "(//div[contains(@class,'course-filter')])[1]//button";
    private String CATEGORY_OPTION = "//a[@href='/courses/category/%s']";
    private String URL = "classes";
    private String SPINNER = "XPATH=>.//*[@class='spinner']";
    private String UNITS_LIST = ".//div[@class='col-12 col-xs-12 col-sm-6 col-md-6 col-lg-4 units-page__unit-item']";
    private String ARABIC_B_CLASS_LINK = "xpath=>.//*[@class='class-selector__class-name' and text()='Arabits (Arabic_B)']";

    /***
     * Methods
     */
    public void clickArabicBClass() {
        waitForElement(ARABIC_B_CLASS_LINK,3);
        javascriptClick(ARABIC_B_CLASS_LINK,"Arabits B Classes link");
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(".//*[@class='spinner']")));
    }

    public boolean isOpen() {
        return driver.getCurrentUrl().contains(URL);
    }

    public int unitsCount() {
        List<WebElement> unitsList = driver.findElements(By.xpath(UNITS_LIST));
        return unitsList.size();
    }

    public boolean verifyUnits() {
        boolean result = false;
        if (unitsCount() > 0) {
            result = true;
        }
        result = isOpen() && result;
        return result;
    }

    public boolean verifyUnitCounts(int expectedCount) {
        return unitsCount() == expectedCount;
    }

    public void select(String categoryName) {
        // Find category dropdown
        WebElement categoryDropdown = driver.findElement(By.xpath(CATEGORY_DROPDOWN));
        categoryDropdown.click();
        // Wait for the element to be clickable
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));

        WebElement categoryOption = wait.until(
                ExpectedConditions.elementToBeClickable(
                        By.xpath(String.format(CATEGORY_OPTION, categoryName))));
        // Used JavaScript click because this element was having issues with usual click method
        js.executeScript("arguments[0].click();", categoryOption);
    }

    public int findCoursesCount(String categoryName) {
        // Find category dropdown
        WebElement categoryDropdown = driver.findElement(By.xpath(CATEGORY_DROPDOWN));
        categoryDropdown.click();
        // Wait for the element to be clickable
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(3));
        WebElement categoryOption = wait.until(
                ExpectedConditions.elementToBeClickable(By.xpath(String.format(CATEGORY_OPTION, categoryName))));
        // Get category text
        String categoryText = categoryOption.getText();
        // Split on ( symbol
        // Example: Software IT (2)
        // Value of arrayTemp[0] ->Software IT
        // Value of arrayTemp[1] ->2)
        String[] arrayTemp = categoryText.split("\\(");
        // Split arrayTemp[1] on ) symbol
        // Value of [0] ->2
        String courseCountString = arrayTemp[1].split("\\)")[0];
        int courseCount = Integer.parseInt(courseCountString);
        // Click the dropdown again to close the menu
        categoryDropdown.click();
        return courseCount;
    }

}
