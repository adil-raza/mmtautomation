package com.mmt.testclasses;

import com.mmt.base.BaseTest;
import com.mmt.base.CheckPoint;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

    @Test
    public void verifyLogin() {
        boolean verifyAccountIconImage = homepage.isUserLoggedIn();
//        Assert.assertTrue(verifyAccountIconImage); // code stops, I fix this
        CheckPoint.markFinal("test-01", verifyAccountIconImage, "Account Icon");
    }

    @Test
    public void verifyUnits() {
        classespage = homepage.clickClassesTab();
        classespage.clickArabicBClass();
        Boolean status = classespage.verifyUnitCounts(11);
        CheckPoint.mark("test-02", status, "Unit counts");
        boolean result = classespage.verifyUnits();
        CheckPoint.markFinal("test-02", result, "Verify units are present");
    }
}
