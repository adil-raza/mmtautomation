package com.mmt.login;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.Duration;

public class logintest {

    WebDriver driver;
    String baseURL;

    @BeforeClass
    public void beForeClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        baseURL = "https://demo.alefed.com/?1636901376246#/home";
        driver.get(baseURL);
    }

    @Test
    public void testLogin() {
        driver.findElement(By.id("username")).clear();
        driver.findElement(By.id("username")).sendKeys("s-arabits4@alef.ae");
        driver.findElement(By.id("password")).clear();
        driver.findElement(By.id("password")).sendKeys("12345");
        driver.findElement(By.xpath(".//button[@Type='submit']")).click();
        WebElement welcomeImage = null;
        try {
            welcomeImage = driver.findElement(By.xpath(".//div[@class='home-page-header__message']"));
        } catch (Exception e) {
            e.printStackTrace();
        }
        Assert.assertNotNull(welcomeImage);
        driver.quit();
    }
}
