package com.mmt.base;

import com.mmt.pageclasses.ClassesPage;
import com.mmt.pageclasses.HomePage;
import com.mmt.pageclasses.LoginPage;
import com.mmt.utilities.Constants;
import com.mmt.utilities.Util;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class BaseTest {

    public WebDriver driver;
    protected String baseURL;
    protected HomePage homepage;
    protected LoginPage login;
    protected ClassesPage classespage;
    protected Util util;

    // Pass browser parameter to grtDriver() method in line 30 to run tests from testNg.xml file

    @BeforeClass
    @Parameters({"browser"})
    public void commonSetUp(@Optional("IamOptional") String browser) {
//        WebDriverManager.chromedriver().setup();
//        driver = new ChromeDriver();
        driver = WebDriverFactory.getInstance().getDriver(Constants.BROWSER);
        driver.get(Constants.BASE_URL);
        login = new LoginPage(driver);
    }

    @BeforeMethod
    public void beforeMethod() {
        CheckPoint.clearHashMap();
        homepage = login.signInWith(Constants.DEFAULT_USERNAME, Constants.DEFAULT_PASSWORD);
    }

    @AfterMethod
    public void afterMethod() {
        if (homepage.isUserLoggedIn()) {
            homepage.logOut();
        }
        util.sleep(3000,"Wait for Logout");
    }

    @AfterClass
    public void commonTearDown() {
        WebDriverFactory.getInstance().quitDriver();
    }
}
